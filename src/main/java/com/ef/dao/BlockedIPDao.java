package com.ef.dao;

import java.sql.SQLException;
import java.util.Collection;

public interface BlockedIPDao {

    void saveIps(Collection<String> ip, String blockReason) throws SQLException;
}
