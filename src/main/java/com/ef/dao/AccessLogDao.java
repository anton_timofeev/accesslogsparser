package com.ef.dao;

import com.ef.model.AccessLogDTO;

import java.sql.SQLException;
import java.util.Collection;
import java.sql.Date;

public interface AccessLogDao {
    /**
     * Stores Access Logs from passed in collection into DB.
     *
     * @param logs
     */
    void saveLogs(Collection<AccessLogDTO> logs) throws SQLException;

    /**
     * Imports CSV file into DB
     * @param file
     */
    void importLogs(String file) throws SQLException;

    /**
     * Finds IPs that made more than 100 requests starting from startDate
     *
     * @param startDate
     * @param endDate
     * @param threshold
     * @return found IPs
     */
    Collection<String> findThresholdExcess(Date startDate, Date endDate, int threshold)
            throws SQLException;
}
