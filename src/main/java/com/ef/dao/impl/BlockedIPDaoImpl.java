package com.ef.dao.impl;

import com.ef.dao.BlockedIPDao;
import com.ef.dao.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

public class BlockedIPDaoImpl extends BaseDaoImpl implements BlockedIPDao {

    private static BlockedIPDao INSTANCE;

    private static final String ADD_BLOCKED_IP_QUERY =
            "INSERT INTO BLOCKED_IPS (IP, COMMENT) " +
                    " VALUES (?, ?)";

    private BlockedIPDaoImpl() {
    }

    @Override
    public void saveIps(Collection<String> ips, String blockReason) throws SQLException {
        PreparedStatement statement = null;
        Connection connection = DataSource.getConnection();
        try {
            PreparedStatement saveIpStmnt = connection.prepareStatement(ADD_BLOCKED_IP_QUERY);
            statement = saveIpStmnt;
            ips.forEach(ip -> {
                try {
                    saveIpStmnt.setString(1, ip);
                    saveIpStmnt.setString(2, blockReason);
                    saveIpStmnt.addBatch();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
            saveIpStmnt.executeBatch();
            connection.commit();
        } catch (Exception ex) {
            connection.rollback();
            throw ex;
        } finally {
            closeQuitely(statement);
        }
    }

    public static BlockedIPDao getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new BlockedIPDaoImpl();
        }
        return INSTANCE;
    }
}
