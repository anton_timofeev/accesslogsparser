package com.ef.dao.impl;

public class BaseDaoImpl {


    protected void closeQuitely(AutoCloseable autoCloseable) {
        if (autoCloseable != null) {
            try {
                autoCloseable.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
