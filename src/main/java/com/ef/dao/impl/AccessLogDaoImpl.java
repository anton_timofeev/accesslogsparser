package com.ef.dao.impl;

import com.ef.config.AppContext;
import com.ef.dao.AccessLogDao;
import com.ef.dao.DataSource;
import com.ef.model.AccessLogDTO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class AccessLogDaoImpl extends BaseDaoImpl implements AccessLogDao {

    private static AccessLogDao INSTANCE;

    private static final String CREATE_ACCESS_LOG_QUERY =
            "INSERT INTO ACCESS_LOGS (ACCESS_DATETIME, IP, REQUEST_STATUS, USER_AGENT_INFO, SESSION_ID) " +
                    " VALUES (?, ?, ?, ?, ?)";

    private static final String IMPORT_ACCESS_LOGS_QUERY =
            " LOAD DATA INFILE ? " +
                    " INTO TABLE ACCESS_LOGS " +
                    " FIELDS TERMINATED BY '|'" +
                    " OPTIONALLY ENCLOSED BY '\"'" +
                    " LINES TERMINATED BY '\\r\'" +
                    // " IGNORE 1 LINES" +
                    " (ACCESS_DATETIME, IP, REQUEST_INFO, REQUEST_STATUS, USER_AGENT_INFO)" +
                    " SET SESSION_ID = ?";

    private static final String TRESHOLD_EXCESS_FIND_QUERY =
            "SELECT IP, COUNT(IP) FROM ACCESS_LOGS WHERE ACCESS_DATETIME BETWEEN ? AND ? AND SESSION_ID = ?" +
                    " GROUP BY IP " +
                    " HAVING COUNT(IP) > ? ";

    private AccessLogDaoImpl() {
    }

    @Override
    public void saveLogs(Collection<AccessLogDTO> logs) throws SQLException {
        PreparedStatement statement = null;
        Connection connection = DataSource.getConnection();
        try {
            PreparedStatement saveLogsStmnt = connection.prepareStatement(CREATE_ACCESS_LOG_QUERY);
            statement = saveLogsStmnt;
            logs.forEach(log -> {
                try {
                    saveLogsStmnt.setTimestamp(1, Timestamp.valueOf(log.getAccessDateTime()));
                    saveLogsStmnt.setString(2, log.getIp());
                    saveLogsStmnt.setInt(3, log.getStatus());
                    saveLogsStmnt.setString(4, log.getUserAgentInfo());
                    saveLogsStmnt.setString(5, AppContext.getSessionId());
                    saveLogsStmnt.addBatch();
                } catch (SQLException e) {
                    try {
                        connection.rollback();
                    } catch (SQLException ex) {
                        throw new RuntimeException(ex);
                    }
                    throw new RuntimeException(e);
                }
            });
            saveLogsStmnt.executeBatch();
            connection.commit();
        } catch (Exception ex) {
            connection.rollback();
            throw ex;
        } finally {
            closeQuitely(statement);
            connection.close();
        }
    }

    @Override
    public void importLogs(String file) throws SQLException {
        PreparedStatement statement = null;
        Connection connection = DataSource.getConnection();

        try {
            PreparedStatement importLogsStmnt = connection.prepareStatement(IMPORT_ACCESS_LOGS_QUERY);
            statement = importLogsStmnt;
            importLogsStmnt.setString(1, file);
            importLogsStmnt.setString(2, AppContext.getSessionId());
            importLogsStmnt.executeUpdate();
            connection.commit();
        } catch (Exception ex) {
            connection.rollback();
            throw ex;
        } finally {
            closeQuitely(statement);
            connection.close();
        }
    }

    @Override
    public Collection<String> findThresholdExcess(Date startDate, Date endDate, int threshold)
            throws SQLException {
        PreparedStatement statement = null;
        ResultSet result = null;
        Connection connection = DataSource.getConnection();
        try {
            PreparedStatement findStatement = connection.prepareStatement(TRESHOLD_EXCESS_FIND_QUERY);
            statement = findStatement;
            findStatement.setTimestamp(1, new Timestamp(startDate.getTime()));
            findStatement.setTimestamp(2, new Timestamp(endDate.getTime()));
            findStatement.setString(3, AppContext.getSessionId());
            findStatement.setInt(4, threshold);
            result = findStatement.executeQuery();

            List<String> ips = new LinkedList<>();
            while (result.next()) {
                String ip = result.getString(1);
                ips.add(ip);
            }
            return ips;
        } finally {
            closeQuitely(result);
            closeQuitely(statement);
            connection.close();
        }
    }

    public static AccessLogDao getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AccessLogDaoImpl();
        }
        return INSTANCE;
    }
}
