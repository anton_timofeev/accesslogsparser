package com.ef.dao;

import com.ef.config.ApplicationProperties;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {
    private static ComboPooledDataSource pooledDataSource = new ComboPooledDataSource();

    static {
        ApplicationProperties properties = ApplicationProperties.getInstance();
        try {
            pooledDataSource.setDriverClass(properties.databaseDriver());
            pooledDataSource.setJdbcUrl(properties.databaseURL());
            pooledDataSource.setUser(properties.databaseUser());
            pooledDataSource.setPassword(properties.databasePassword());
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        Connection connection = pooledDataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    private DataSource(){}
}
