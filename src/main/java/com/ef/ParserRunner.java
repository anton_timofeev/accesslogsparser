package com.ef;

import com.ef.config.AppContext;
import com.ef.model.Duration;
import com.ef.service.AccessLogsImportService;
import com.ef.service.LogsValidationService;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.Collection;

public class ParserRunner {
    private static final Logger LOGGER = Logger.getLogger(ParserRunner.class);

    private static ParserRunner INSTANCE;

    private AccessLogsImportService accessLogsImportService;

    private LogsValidationService logsValidationService;

    private ParserRunner() {
    }

    public void execute(String file, Date startDate, Duration duration, int threshold) {
        LOGGER.info("Starting parsing/validation process within session: " + AppContext.getSessionId());
        if (file == null) {
            LOGGER.info("No file was passed. Exiting.");
            return;
        }
        // search for IP to block only when importing passed successfully
        if (accessLogsImportService.doImport(file)) {
            Collection<String> blockedIps = logsValidationService.doValidate(startDate, duration, threshold);

            blockedIps.forEach(ip -> LOGGER.info(ip + " IP has been blocked due to request amount excess threshold."));
        } else {
            LOGGER.info("Import failed. Check previous logs.");
        }
    }

    public void setAccessLogsImportService(AccessLogsImportService accessLogsImportService) {
        this.accessLogsImportService = accessLogsImportService;
    }

    public void setLogsValidationService(LogsValidationService logsValidationService) {
        this.logsValidationService = logsValidationService;
    }

    public static synchronized ParserRunner getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ParserRunner();
        }
        return INSTANCE;
    }
}
