package com.ef.config;

import java.util.UUID;

public class AppContext {
    private static final ThreadLocal<String> SESSION_ID
            = ThreadLocal.withInitial(() -> UUID.randomUUID().toString());

    public static String getSessionId() {
        return SESSION_ID.get();
    }
}
