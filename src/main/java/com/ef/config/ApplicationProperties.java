package com.ef.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Application properties container.
 */
public class ApplicationProperties {

    private static ApplicationProperties INSTANCE;

    private static final Properties PROPERTIES = new Properties();

    static {
        try {
            // firstly trying load properties file in the same directory where jar is placed
            // if there is no properties file - loads it from Jar itself
            PROPERTIES.load(
                    new FileInputStream("./application.properties"));
        } catch (IOException ex) {
            try {
                PROPERTIES.load(
                        ApplicationProperties.class.getClassLoader()
                                .getResourceAsStream("application.properties"));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public static ApplicationProperties getInstance() {
        if (INSTANCE != null) {
            return INSTANCE;
        }
        INSTANCE = new ApplicationProperties();
        return INSTANCE;
    }

    private ApplicationProperties() {

    }

    public String getInputFilePath() {
        return PROPERTIES.getProperty("default.input.file.path");
    }

    public String databaseDriver() {
        return PROPERTIES.getProperty("db.driver");
    }

    public String databaseURL() {
        return PROPERTIES.getProperty("db.url");
    }

    public String databaseUser() {
        return PROPERTIES.getProperty("db.user");
    }

    public String databasePassword() {
        return PROPERTIES.getProperty("db.password");
    }
}
