package com.ef.config;

import com.ef.ParserRunner;
import com.ef.dao.AccessLogDao;
import com.ef.dao.BlockedIPDao;
import com.ef.dao.impl.AccessLogDaoImpl;
import com.ef.dao.impl.BlockedIPDaoImpl;
import com.ef.service.AccessLogsImportService;
import com.ef.service.LogsValidationService;
import com.ef.service.impl.LogsValidationServiceImpl;
import com.ef.service.impl.ParseAccessLogsImportServiceImpl;

/**
 * Implementation of IoC
 */
public class ParsingAppConfiguration {

    public static void init() {
        // DAOs
        AccessLogDao accessLogDao = AccessLogDaoImpl.getInstance();

        BlockedIPDao blockedIPDao = BlockedIPDaoImpl.getInstance();

        // services
        AccessLogsImportService parserService = ParseAccessLogsImportServiceImpl.getInstance();
        parserService.setAccessLogDao(accessLogDao);

        LogsValidationService logsValidationService = LogsValidationServiceImpl.getInstance();
        logsValidationService.setAccessLogDao(accessLogDao);
        logsValidationService.setBlockedIPDao(blockedIPDao);

        ParserRunner parserRunner = ParserRunner.getInstance();
        parserRunner.setAccessLogsImportService(parserService);
        parserRunner.setLogsValidationService(logsValidationService);

        AppConfiguration.putBean(AccessLogDao.class, accessLogDao);
        AppConfiguration.putBean(BlockedIPDao.class, blockedIPDao);
        AppConfiguration.putBean(AccessLogsImportService.class, parserService);
        AppConfiguration.putBean(LogsValidationService.class, logsValidationService);
        AppConfiguration.putBean(ParserRunner.class, parserRunner);
    }

}
