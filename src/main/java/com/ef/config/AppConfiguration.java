package com.ef.config;

import java.util.HashMap;
import java.util.Map;

public class AppConfiguration {
    private static Map<Class, Object> BEANS = new HashMap<>();

    public static <T> T getBean(Class<T> clazz) {
        return (T) BEANS.get(clazz);
    }

    public static <T> void putBean(Class<T> clazz, Object bean) {
        BEANS.put(clazz, bean);
    }
}
