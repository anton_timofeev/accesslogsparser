package com.ef.service;

import com.ef.dao.AccessLogDao;
import com.ef.dao.BlockedIPDao;
import com.ef.model.Duration;

import java.sql.Date;
import java.util.Collection;

public interface LogsValidationService {
    /**
     * Performs validation of access logs for request excess threshold. If some IPs
     * haven't passed validation they are stored in DB.
     *
     * @param startDate starting date time
     * @param duration daily/hourly time period to validate logs in
     * @return list of IPs from which there were more requests than threshold
     */
    Collection<String> doValidate(Date startDate, Duration duration, int threshold);

    void setAccessLogDao(AccessLogDao accessLogDao);

    void setBlockedIPDao(BlockedIPDao blockedIpDao);
}
