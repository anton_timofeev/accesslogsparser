package com.ef.service;

import com.ef.dao.AccessLogDao;

public interface AccessLogsImportService {

    /**
     * Reads input file and parses its each line and stores access logs into DB.
     *
     * @param file
     * @return {@code true} - if importing was finished successfully
     */
    // TODO: add some progress visualisation
    boolean doImport(String file);

    void setAccessLogDao(AccessLogDao logService);
}
