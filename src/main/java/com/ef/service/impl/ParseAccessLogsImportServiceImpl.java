package com.ef.service.impl;

import com.ef.dao.AccessLogDao;
import com.ef.model.AccessLogDTO;
import com.ef.service.AccessLogsImportService;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Access Logs Importing implementation via parsing input file and pushing
 * parsed objects into DB.
 */
public class ParseAccessLogsImportServiceImpl implements AccessLogsImportService {
    private static final Logger LOGGER = Logger.getLogger(ParseAccessLogsImportServiceImpl.class);

    private static final int LOGS_LINES_THRESHOLD = 500;
    private static final String LINE_PATTERN_DEFINITION =
            "([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{3})" + // group 1: date and time
                    "\\|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})" + // group 2: IP
                    "\\|([^\\|]+)" + // group 3: requestInfo
                    "\\|([0-9]{1,3})" + // group 4: request status
                    "\\|([.\\s\\S]+)";  // group 5: user agent info
    private static final Pattern LINE_PATTERN = Pattern.compile(LINE_PATTERN_DEFINITION);

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private AccessLogDao accessLogDao;
    private static AccessLogsImportService INSTANCE;

    private ParseAccessLogsImportServiceImpl() {
    }

    public boolean doImport(String file) {
        LOGGER.debug("Starting file importing via parsing by lines.");
        List<AccessLogDTO> logs = new LinkedList<>();
        LOGGER.info("Importing in-progress...");
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                Matcher lineMatcher = LINE_PATTERN.matcher(line);
                if (lineMatcher.matches()) {
                    String timestamp = lineMatcher.group(1);
                    String ip = lineMatcher.group(2);
                    String status = lineMatcher.group(4);
                    String userAgentInfo = lineMatcher.group(5);

                    logs.add(new AccessLogDTO().setIp(ip)
                            .setAccessDateTime(timestamp)
                            .setUserAgentInfo(userAgentInfo).setStatus(Integer.valueOf(status)));
                }
                if (logs.size() >= LOGS_LINES_THRESHOLD) {
                    saveAccessLogs(logs);
                    LOGGER.info("Importing " + logs.size() + " rows...");
                    logs.clear();
                }
            }
            if (!logs.isEmpty()) {
                saveAccessLogs(logs);
                LOGGER.info("Importing " + logs.size() + " rows.");
            }
            LOGGER.info("Importing has been finished.");
            logs.clear();
        } catch (FileNotFoundException ex) {
            LOGGER.error("Importing file is failed due missing file.");
            return false;
        } catch (IOException | SQLException e) {
            LOGGER.error("Importing file is failed due to:", e);
            return false;
        }
        return true;
    }

    private void saveAccessLogs(Collection<AccessLogDTO> logs) throws SQLException {
        accessLogDao.saveLogs(logs);
    }

    @Override
    public void setAccessLogDao(AccessLogDao accessLogDao) {
        this.accessLogDao = accessLogDao;
    }

    public static AccessLogsImportService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ParseAccessLogsImportServiceImpl();
        }
        return INSTANCE;
    }
}
