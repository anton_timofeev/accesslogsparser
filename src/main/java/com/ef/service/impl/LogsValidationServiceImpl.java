package com.ef.service.impl;

import com.ef.dao.AccessLogDao;
import com.ef.dao.BlockedIPDao;
import com.ef.model.Duration;
import com.ef.service.LogsValidationService;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;

public class LogsValidationServiceImpl implements LogsValidationService {
    private static final Logger LOGGER = Logger.getLogger(LogsValidationServiceImpl.class);

    private AccessLogDao accessLogDao;

    private BlockedIPDao blockedIPDao;

    private static LogsValidationService INSTANCE;

    @Override
    public Collection<String> doValidate(Date startDate, Duration duration, int threshold) {
        LOGGER.info("Starting validation of IPs threshold excess.");
        if (duration == null) {
            throw new IllegalArgumentException("Duration should be not null");
        }
        Date endDate = getEndDateValue(startDate, duration);
        Collection<String> ips = Collections.emptyList();
        try {
            ips = accessLogDao.findThresholdExcess(startDate, endDate, threshold);

            if (!ips.isEmpty()) {
                LOGGER.info("Found " + ips.size() + " IPs from which there were more than allowed threshold request amount.");
                String blockingMessage = "Blocked due to threshold (" + threshold +") excess.";
                blockedIPDao.saveIps(ips, blockingMessage);
            } else {
                LOGGER.info("No IP excess threshold found");
            }
        } catch (SQLException e) {
            LOGGER.error("Search for IPs from which there were more than allowed threshold request amount failed due to: " , e);
        }

        return ips;
    }

    private Date getEndDateValue(Date startDate, Duration duration) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        switch (duration) {
            case DAILY:
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                break;
            case HOURLY:
                calendar.add(Calendar.HOUR_OF_DAY, 1);
                break;
        }
        calendar.add(Calendar.SECOND, -1);
        return new Date(calendar.getTimeInMillis());
    }

    @Override
    public void setAccessLogDao(AccessLogDao accessLogDao) {
        this.accessLogDao = accessLogDao;
    }

    @Override
    public void setBlockedIPDao(BlockedIPDao blockedIPDao) {
        this.blockedIPDao = blockedIPDao;
    }

    public static LogsValidationService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LogsValidationServiceImpl();
        }
        return INSTANCE;
    }
}
