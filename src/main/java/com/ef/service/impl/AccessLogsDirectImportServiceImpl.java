package com.ef.service.impl;

import com.ef.dao.AccessLogDao;
import com.ef.service.AccessLogsImportService;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * Access Logs Import implementation via direct importing CSV file into DB
 */
public class AccessLogsDirectImportServiceImpl implements AccessLogsImportService {
    private static final Logger LOGGER = Logger.getLogger(AccessLogsDirectImportServiceImpl.class);

    private AccessLogDao accessLogDao;
    private static AccessLogsImportService INSTANCE;

    private AccessLogsDirectImportServiceImpl() {
    }

    public boolean doImport(String file) {
        return importAccessLogs(file);
    }

    private boolean importAccessLogs(String file) {
        LOGGER.info("Starting CSV file importing.");
        try {
            accessLogDao.importLogs(file);
            LOGGER.info("CSV file importing has been finished");
        } catch (SQLException e) {
            LOGGER.error("CSV file importing failed due to: ", e);
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public void setAccessLogDao(AccessLogDao accessLogDao) {
        this.accessLogDao = accessLogDao;
    }

    public static AccessLogsImportService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AccessLogsDirectImportServiceImpl();
        }
        return INSTANCE;
    }
}
