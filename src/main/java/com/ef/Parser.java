package com.ef;

import com.ef.config.FileImportAppConfiguration;
import com.ef.config.ParsingAppConfiguration;
import com.ef.model.Duration;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Parser {
    private static final Logger LOGGER = Logger.getLogger(Parser.class);
    private static final String DATE_ARG_DATE_FORMAT = "yyyy-MM-dd.HH:mm:ss";
    private static final DateFormat SDF = new SimpleDateFormat(DATE_ARG_DATE_FORMAT);
    private static final String START_DATE_ARG_NAME = "startDate";
    private static final String DURATION_ARG_NAME = "duration";
    private static final String THRESHOLD_ARG_NAME = "threshold";
    private static final String ACCESSLOG_ARG_NAME = "accesslog";
    private static final String IMPORT_CSV_ARG_NAME = "importCSV";

    public static void main(String[] args) {
        CommandLine cmd = parseCommandLineArgs(args);
        Date startDateValue = parseStartDate(cmd);
        Duration durationValue = parseDurationValue(cmd);
        Integer thresholdValue = parseThresholdValue(cmd);
        String file = cmd.getOptionValue(ACCESSLOG_ARG_NAME);

        Boolean isImportDirectly = cmd.hasOption(IMPORT_CSV_ARG_NAME);
        if (isImportDirectly) {
            FileImportAppConfiguration.init();
        } else {
            ParsingAppConfiguration.init();
        }
        ParserRunner.getInstance().execute(file, startDateValue, durationValue, thresholdValue);
    }

    private static Integer parseThresholdValue(CommandLine cmd) {
        Integer thresholdValue = null;
        try {
            thresholdValue = Integer.valueOf(cmd.getOptionValue(THRESHOLD_ARG_NAME));
        } catch (NumberFormatException ex) {
            LOGGER.error("Threshold argument value is not integer.");
            System.exit(1);
        }
        return thresholdValue;
    }

    private static Duration parseDurationValue(CommandLine cmd) {
        Duration durationValue = null;
        try {
             durationValue = Duration.valueOf(cmd.getOptionValue(DURATION_ARG_NAME).toUpperCase());
        } catch (IllegalArgumentException ex) {
            LOGGER.error("Duration argument has invalid value. Only 'daily' or 'hourly' is allowed");
            System.exit(1);
        }
        return durationValue;
    }

    private static Date parseStartDate(CommandLine cmd) {
        Date startDateValue = null;
        try {
            startDateValue = new Date(SDF.parse(cmd.getOptionValue(START_DATE_ARG_NAME)).getTime());
        } catch (java.text.ParseException e) {
            LOGGER.error("startDate argument value has wrong format. Check valid format with --help argument");
            System.exit(1);
        }
        return startDateValue;
    }

    private static CommandLine parseCommandLineArgs(String[] args) {
        Options options = new Options();

        Option startDate = new Option(START_DATE_ARG_NAME, START_DATE_ARG_NAME,
                true, DATE_ARG_DATE_FORMAT);
        startDate.setRequired(true);
        startDate.setValueSeparator('=');
        options.addOption(startDate);

        Option duration = new Option(DURATION_ARG_NAME, DURATION_ARG_NAME,
                true, "can take only 'hourly', 'daily' as inputs");
        duration.setRequired(true);
        duration.setValueSeparator('=');
        options.addOption(duration);

        Option threshold = new Option(null, THRESHOLD_ARG_NAME,
                true, "can be any integer");
        threshold.setRequired(true);
        threshold.setValueSeparator('=');
        options.addOption(threshold);

        Option accesslog = new Option(ACCESSLOG_ARG_NAME, ACCESSLOG_ARG_NAME,
                true, "input file");
        accesslog.setValueSeparator('=');
        accesslog.setRequired(false);
        options.addOption(accesslog);

        Option importDirectly = new Option("i", IMPORT_CSV_ARG_NAME,
                true, "Enable importing CSV file directly to DB. " +
                " Suitable in case input file has following similar line format: " +
                " 2017-01-01 00:00:21.164|192.168.234.82|\"GET / HTTP/1.1\"|200|\"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0\"");
        importDirectly.setRequired(false);
        importDirectly.setArgs(0);
        importDirectly.setType(Boolean.class);
        options.addOption(importDirectly);

        CommandLineParser parser = new GnuParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Server Access Logs parser", options);
            System.exit(1);
        }
        return cmd;
    }
}
