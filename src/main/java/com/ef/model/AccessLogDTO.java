package com.ef.model;

public class AccessLogDTO {
    private String accessDateTime;

    private String ip;

    private Integer status;

    private String userAgentInfo;

    public Integer getStatus() {
        return status;
    }

    public AccessLogDTO setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getAccessDateTime() {
        return accessDateTime;
    }

    public AccessLogDTO setAccessDateTime(String accessDateTime) {
        this.accessDateTime = accessDateTime;
        return this;
    }

    public String getIp() {
        return ip;
    }

    public AccessLogDTO setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public String getUserAgentInfo() {
        return userAgentInfo;
    }

    public AccessLogDTO setUserAgentInfo(String userAgentInfo) {
        this.userAgentInfo = userAgentInfo;
        return this;
    }
}
